package gittutorial;

public class HelloNameTemplate {

	public static void main(String[] args) {
		System.out.println("Hello " + sendName());
		System.out.println("Evan is cool!");
	}
	
	public static String sendName() {
		return "Evan Nielsen";
	}

}
